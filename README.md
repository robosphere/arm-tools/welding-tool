# Welding tool

сварочная голова ver0.3
![connection-toole](img/welding2.png)

сварочная голова ver0.2
![connection-toole](img/welding1.png)

подача материала из перемещающейся тележки ver0.1
![connection-toole](img/подача.png)

ver01

[![Welding tool discuss](img/welding_machine.png)
Приспособление для сварки контактов

## Обсуждение первой версии приспособления (video)
[![Welding tool discuss](https://img.youtube.com/vi/C1FLd-AEGv8/0.jpg)](https://www.youtube.com/watch?v=C1FLd-AEGv8 "Welding tool discuss")

С информация по проблеммам наплавления металла и их решению можно ознакомиться в файле **сварка.doc**
